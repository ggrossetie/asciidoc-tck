import { describe, it } from 'node:test'
import assert from 'node:assert'
import resolveCliArgs from '../lib/cli.js'

describe('resolveCliArgs()', () => {
  it('should parse cli shorthand format', () => {
    const opts = resolveCliArgs(['cli', '/path/to/cmd'])
    assert.deepEqual(opts, {
      adapter: {
        cli: {
          command: '/path/to/cmd',
        },
      },
    })
  })
  it('should parse cli shorthand format using a single positional argument', () => {
    const opts = resolveCliArgs(['/path/to/cmd'])
    assert.deepEqual(opts, {
      adapter: {
        cli: {
          command: '/path/to/cmd',
        },
      },
    })
  })
  it('should parse server arguments with url', () => {
    const opts = resolveCliArgs(['server', '-u', 'http://localhost:1234/asciidoc-tck'])
    assert.deepEqual(opts, {
      adapter: {
        server: {
          url: 'http://localhost:1234/asciidoc-tck',
        },
      },
    })
  })
  it('should parse server arguments with url and stop command', () => {
    const opts = resolveCliArgs([
      'server',
      '--url=http://localhost:1234/asciidoc-tck',
      '--stop-command=/path/to/stop-server',
    ])
    assert.deepEqual(opts, {
      adapter: {
        server: {
          url: 'http://localhost:1234/asciidoc-tck',
          stopCommand: '/path/to/stop-server',
        },
      },
    })
  })
  it('should parse server arguments with start and stop commands', () => {
    const opts = resolveCliArgs([
      'server',
      '--start-command=/path/to/start-server',
      '--stop-command=/path/to/stop-server',
    ])
    assert.deepEqual(opts, {
      adapter: {
        server: {
          startCommand: '/path/to/start-server',
          stopCommand: '/path/to/stop-server',
        },
      },
    })
  })
  it('should throw an error when neither --url nor --start-command is defined', () => {
    assert.throws(() => resolveCliArgs(['server']), {
      message: 'You must specify either specify a start command using --start-command or an URL using --url or -u',
    })
  })
  it('should throw an error when adapter mode is unrecognized', () => {
    assert.throws(() => resolveCliArgs(['proxy', '/path/to/cmd']), {
      message: 'The TCK adapter mode must be either "cli" or "server"',
    })
  })
  it('should throw an error when argument is unrecognized', () => {
    assert.throws(() => resolveCliArgs(['cli', '--unknown=foo']), {
      message:
        "Unknown option '--unknown'. To specify a positional argument starting with a '-', place it at the end of the command after '--', as in '-- \"--unknown\"",
    })
  })
  it('should throw an error when argument value is missing', () => {
    assert.throws(() => resolveCliArgs(['cli', '--adapter-command']), {
      message: "Option '-c, --adapter-command <value>' argument missing",
    })
  })
  it('should throw an error when argument value is missing', () => {
    assert.throws(() => resolveCliArgs(['cli', '-c']), {
      message: "Option '-c, --adapter-command <value>' argument missing",
    })
  })
  it('should parse command with tests-dir', () => {
    const opts = resolveCliArgs(['cli', '--adapter-command=/path/to/cmd', '--tests=/path/to/tests'])
    assert.deepEqual(opts, {
      testsDir: '/path/to/tests',
      adapter: {
        cli: {
          command: '/path/to/cmd',
        },
      },
    })
  })
})
