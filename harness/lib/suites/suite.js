import ospath from 'node:path'
import { before, after, describe, makeTests, populateASGDefaults, assert, stringifyASG } from '../test-framework.js'
import { createAdapterManager } from '../adapter-manager.js'
import loadTests from '../test-loader.js'
import { toModuleContext } from '../helpers.js'

const PACKAGE_DIR = ospath.join(toModuleContext(import.meta).__dirname, '../../..')
const adapterManager = createAdapterManager()

async function parse (input) {
  return adapterManager.post(input)
}
const testsSuite = async (testDir = ospath.join(PACKAGE_DIR, 'tests')) => {
  const tests = await loadTests(testDir)
  await describe('tests', () => {
    before(async () => {
      await adapterManager.start()
    })
    after(async () => {
      await adapterManager.stop()
    })
    describe('inline', function () {
      const inlineTests = tests.find((it) => it.type === 'dir' && it.name === 'inline').entries
      makeTests(inlineTests, async function ({ input, inputPath, expected, expectedWithoutLocations }) {
        const actualRoot = await parse({
          contents: input,
          path: inputPath,
          type: 'inline',
        })
        assert.ok('blocks' in actualRoot, 'should have a blocks property')
        assert.ok(actualRoot.blocks.length === 1, 'blocks property should have one (and only one) block')
        assert.ok('inlines' in actualRoot.blocks[0], 'first block in blocks should have an inlines property')
        const actual = actualRoot.blocks[0].inlines
        if (expected == null) {
          // Q: can we write data to expected file automatically?
          // TODO only output expected if environment variable is set
          console.log(stringifyASG(actual))
          this.skip()
        } else {
          const msg = `actual output does not match expected output for ${inputPath}`
          // TODO wrap this in expectAsg helper
          assert.deepEqual(actual, !actual.length || 'location' in actual[0] ? expected : expectedWithoutLocations, msg)
        }
      })
    })
    describe('block', function () {
      const blockTests = tests.find((it) => it.type === 'dir' && it.name === 'block').entries
      makeTests(blockTests, async function ({ input, inputPath, expected, expectedWithoutLocations }) {
        const actual = await parse({
          contents: input,
          path: inputPath,
          type: 'block',
        })
        if (expected == null) {
          // Q: can we write data to expect file automatically?
          // TODO only output expected if environment variable is set
          console.log(stringifyASG(actual))
          this.skip()
        } else {
          const msg = `actual output does not match expected output for ${inputPath}`
          // TODO wrap this in expectAsg helper
          assert.deepEqual(
            populateASGDefaults(actual),
            populateASGDefaults('location' in actual ? expected : expectedWithoutLocations),
            msg
          )
        }
      })
    })
  })
}

export default testsSuite
