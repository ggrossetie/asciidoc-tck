/* global fetch */
import { exec } from 'node:child_process'

class AdapterServerManager {
  constructor ({ startCommand, stopCommand, url }) {
    this.startCommand = startCommand
    this.stopCommand = stopCommand
    this.url = url
  }

  async post (data) {
    const response = await fetch(this.url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
    return response.json()
  }

  async start () {
    if (this.startCommand !== undefined) {
      const url = await new Promise((resolve, reject) => {
        exec(this.startCommand, (err, stdout, _stderr) => {
          if (err) return reject(err)
          resolve(stdout)
        })
      })
      if (this.url === undefined) {
        this.url = url
      }
    }
  }

  stop () {
    if (this.stopCommand !== undefined) {
      return new Promise((resolve, reject) => {
        exec(this.stopCommand, (err, stdout, _stderr) => {
          if (err) return reject(err)
          resolve(stdout)
        })
      })
    }
  }
}

class AdapterCliManager {
  constructor ({ command }) {
    this.command = command
  }

  post (data) {
    return new Promise((resolve, reject) => {
      const cp = exec(this.command, (err, stdout, _stderr) => {
        if (err) return reject(err)
        try {
          resolve(JSON.parse(stdout))
        } catch (err) {
          reject(err)
        }
      })
      cp.stdin.on('error', (_err) => undefined).end(JSON.stringify(data)) // eslint-disable-line n/handle-callback-err
    })
  }

  start () {}

  stop () {}
}

export function createAdapterManager () {
  const adapterConfig = getAdapterConfig()
  if (adapterConfig.server) {
    return new AdapterServerManager(adapterConfig.server)
  }
  return new AdapterCliManager(adapterConfig.cli)
}

function getAdapterConfig () {
  if (process.env.ASCIIDOC_TCK_ADAPTER_MODE === 'server') {
    const startCommand = process.env.ASCIIDOC_TCK_ADAPTER_SERVER_START_COMMAND
    const stopCommand = process.env.ASCIIDOC_TCK_ADAPTER_SERVER_STOP_COMMAND
    const url = process.env.ASCIIDOC_TCK_ADAPTER_URL
    return {
      server: {
        ...(url && { url }),
        ...(startCommand && { startCommand }),
        ...(stopCommand && { stopCommand }),
      },
    }
  }
  const command = process.env.ASCIIDOC_TCK_ADAPTER_CLI_COMMAND
  return {
    cli: {
      ...(command && { command }),
    },
  }
}
