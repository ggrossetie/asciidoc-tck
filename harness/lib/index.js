import run from './runner.js'
import resolveCliArgs from './cli.js'
import process from 'node:process'
import testSuite from './suites/suite.js'

// rollup does not support top-level await when producing a cjs output
;(async () => {
  const args = process.argv.slice(2)
  if (globalThis.NODE_SEA === 'true' && args[0] === '//asciidoc-tck-test-suite.js') {
    try {
      await testSuite(process.env.ASCIIDOC_TCK_TESTS || './tests')
    } catch (err) {
      console.error('Something went wrong!', err)
      process.exit(1)
    }
  } else {
    let config
    try {
      config = resolveCliArgs()
    } catch (err) {
      console.error('No tests run.')
      console.error(err.message)
      process.exit(1)
    }
    try {
      const { fail } = await run(config)
      process.exit(fail)
    } catch (err) {
      console.error('Something went wrong!', err)
      process.exit(1)
    }
  }
})()
