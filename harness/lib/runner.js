import process from 'node:process'
import { spec } from 'node:test/reporters'
import ospath from 'node:path'
import { run } from './test-framework.js'
import { toModuleContext } from './helpers.js'

const PACKAGE_DIR = ospath.join(toModuleContext(import.meta).__dirname, '../..')

const ac = new AbortController()
ac.signal.addEventListener(
  'abort',
  (event) => {
    console.log('Tests aborted with event: ' + event.type)
  },
  { once: true }
)

/**
 * @param config
 * @returns {Promise<{fail: number, pass: number}>}
 */
export default async (config) => {
  const adapterConfig = config.adapter
  // pass data to the tests runner as environment variables
  if (config.testsDir !== undefined) {
    process.env.ASCIIDOC_TCK_TESTS = config.testsDir
  }
  if (adapterConfig.cli) {
    process.env.ASCIIDOC_TCK_ADAPTER_MODE = 'cli'
    process.env.ASCIIDOC_TCK_ADAPTER_CLI_COMMAND = adapterConfig.cli.command
  } else if (adapterConfig.server) {
    process.env.ASCIIDOC_TCK_ADAPTER_MODE = 'server'
    process.env.ASCIIDOC_TCK_ADAPTER_SERVER_START_COMMAND = adapterConfig.server.startCommand
    process.env.ASCIIDOC_TCK_ADAPTER_SERVER_STOP_COMMAND = adapterConfig.server.stopCommand
    process.env.ASCIIDOC_TCK_ADAPTER_URL = adapterConfig.server.url
  }
  const testFile =
    globalThis.NODE_SEA === 'true'
      ? '//asciidoc-tck-test-suite.js' // virtual file
      : ospath.join(PACKAGE_DIR, 'harness/lib/suites/index.js')
  const testsStream = run({
    files: [testFile],
    signal: ac.signal,
  })
  let fail = 0
  let pass = 0
  const errors = []

  let reporter
  if (config.reporter === false) {
    reporter = () => {}
  } else {
    const specReporter = spec()
    reporter = (event) =>
      specReporter._transform(event, 'utf-8', (_, data) => {
        if (data) {
          process.stdout.write(data)
        }
      })
  }
  for await (const event of testsStream) {
    reporter(event)
    if (event.type === 'error') {
      throw event.data
    }
    if (event.type === 'test:pass' && event.data.details.type !== 'suite') {
      pass += 1
    }
    if (event.type === 'test:fail' && event.data.details.type !== 'suite') {
      fail += 1
      errors.push(event.data.details.error.message)
    }
  }
  return {
    fail,
    pass,
    errors,
  }
}
